#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_spi_flash.h"
#include "esp_spiram.h"
#include "tests.h"
#include "display/dispcolor.h"
#include "icn2053/icn2053.h"
#include "rmt/rmt.h"

#define SW_VERSION_MAJOR	1
#define SW_VERSION_MINOR	0


// ���������� ������
#define dispWidth 				128
#define dispHeight				64


void ledmatrix_task(void* arg)
{
	while (1)
    {
    	icn2053_refresh();
    }
}

void main_task(void* arg)
{
	while (1)
    {
		//ScanTest();
		//RefreshTest();

		//TextTest1();
		//TextTest2();
		//TextTest3();
		//TextTest4();

		//GradientTest();

		RainbowTestInit();
		while (1)
			RainbowTest();

    }
}


//==============================================================================
void app_main()
{
	// ������������� �������
	dispcolor_Init(dispWidth, dispHeight);

    xTaskCreatePinnedToCore(main_task, "Main", 2048, NULL, 9, NULL, 0);
    xTaskCreatePinnedToCore(ledmatrix_task, "LedMatrix", 2048, NULL, 8, NULL, 1);

	while (1)
    {
	    vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}
//==============================================================================
